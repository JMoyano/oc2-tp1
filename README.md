# _Trabajo Practico N°1 - Organizacion del Computador 2 - 2do Cuatrimestre 2021_ 

## Formula resolvente: 

### Consignas: 
1. Realizar un programa para la arquitectura IA32 que calcule las raíces de una función
cuadrática a través de la fórmula resolvente. Los coeficientes a, b y c de la función
deben ser recibidos por parámetro. Considerar que estos podrían tomar valores de
punto flotante o no.

- Esta consigna fue resuelta en el siguiente archivo [formulaResolvente.asm](/formulaResolvente.asm).

2. Hacer un programa en C, que solicite al usuario estos valores a,b y c. Invocar a la
función del punto 1 desde C.

- Esta consigna fue resuelta en el siguiente archivo [formulaResolventeC.c](/formulaResolventeC.c).

3. Compilar y linkear los archivos objeto de manera separada. Obtener un ejecutable que
muestre por consola las raíces obtenidas.

- Para esta consigna se genero un archivo [scriptEjecucionFR.sh](/scriptEjecucionFR.sh) el cual se ejecuta con la siguiente linea: 
`sh scriptEjecucionFR.sh` en una consola linux.

A continuacion se muestra un ejemplo de ejecucion:

![ejecucionFR](/Capturas/FormulaResolvente/ejecucionFR.png)

Dentro del archivo **scriptEjecucionFR.sh** se encuentran los siguientes comandos. 

![datosScript](/Capturas/FormulaResolvente/datosScript.png)

- El comando `nasm -f elf32 ...` realiza la compilacion del archivo .asm en un archivo del tipo .o. 
- El comando `gcc -m32 ...` se encarga de linkear el archivo .o que se genero anteriormente, con el archivo .c y generar un ejecutable. 
- El comando `./main` realiza la ejecucion del archivo ejecutable generado en la linea anterior.

## Ejercicio Extra - Producto escalar

4. Escriba una función en assembler IA-32 que reciba un número r y un puntero a un
vector de números de punto flotante, que calcule el producto escalar. Debe multiplicar
cada elemento del vector por r.

- Este ejercicio fue resuelto en el archivo [productoEscalar.asm](/EjercicioExtra/productoEscalar.asm). En este caso el vector y el numero r son declarados dentro del mismo archivo, no son enviados como parametros desde C.
Aunque fue agregada la forma de recorrer el vector resultante de multiplicar cada valor del vector original por el valor de r e imprimir cada valor.

Los valores iniciales del vector y r son los siguientes: 

![valoresIniciales](/Capturas/EjercicioExtra/valoresIniciales.png)

Luego de la ejecucion el resultado mostrado en pantalla es el siguiente: 

![resultado](/Capturas/EjercicioExtra/resultado.png)

#### Autor 
_**Moyano, Juan Gabriel**_ :computer: - [JMoyano](https://gitlab.com/JMoyano)
