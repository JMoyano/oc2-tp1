%include "io.inc"
extern printf

section .data
vector dd 1.00,2.10,3.50,5.0
cantidad dd 4
producto dd 2.0

format db "%.2f ",0
valorIVector dq 0.0

section .text
global CMAIN 
CMAIN:
    mov ebp, esp; for correct debugging
   
    push cantidad
    push vector
    call producto_rvf ;Llamo al metodo para multiplicar cada valor del vector por el producto
    add esp,8 
     
    push cantidad
    push vector
    call printArray ;Llamo al metodo para imprimir cada valor del vector
    add esp,8 
    
    xor ebx, ebx
    ret
  
producto_rvf:
    push ebp ;enter
    mov ebp, esp  ;enter
    
    xor ebx,ebx ;lo limpio para que ebx actue como contador en el ciclo
    mov eax ,[EBP+8] ;muevo a eax la direccion del vector
    mov edx ,[EBP+12];muevo a edx la direccion de la cantidad de elementos
   
.ciclo: 
    FLD dword[eax +4*ebx] ;ingreso a la FPU el valor para multiplicar por el producto antes definido
    FLD dword[producto] ;ingreso el valor de producto
    FMUL ;multiplico
    FSTP dword[eax +4*ebx] ;extraigo el resultado de la multiplicacion en la misma direccion de memoria de valor original 
    
    inc ebx ;incremento en uno el contador
    
    cmp ebx, [edx]
    jl .ciclo ;si el contador es menor a la cantidad de registros sigo en el ciclo
    
    ;fin del ciclo
    
    mov ebp,esp ;leave
    pop ebp ;leave
    
    ret

printArray:
    push ebp ;enter
    mov ebp, esp  ;enter
    
    xor ebx,ebx ;lo limpio para que ebx actue como contador en el ciclo
    mov eax ,[EBP+8] ;en eax esta la direccion del vector
    mov edx ,[EBP+12];en edx esta la direccion de la cantidad de elementos

.ciclo:
    FLD dword[eax +4*ebx] ;ingreso el valor a la FPU
    
    FSTP qword[valorIVector] ;extraigo de la FPU el valor en qword para poder imprimir
    
    push edx ;pusheo el valor de edx para manenerlo luego del print
    push eax ;pusheo el valor de eax para manenerlo luego del print
    
    push dword[valorIVector+4]
    push dword[valorIVector]
    push format
    call printf
    add esp,12
    
    pop eax ;extraigo el valor de eax luego de ejecutar print
    pop edx ;extraigo el valor de edx luego de ejecutar print
    
    inc ebx ;incremento en uno el contador
    
    cmp ebx, [edx]
    jl .ciclo ;si el contador es menor a la cantidad de registros sigo en el ciclo
    
    ;fin del ciclo
   
    mov ebp,esp ;leave
    pop ebp ;leave
    
    ret