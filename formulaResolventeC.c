#include <stdio.h>
#include <stdlib.h>

extern void formulaResolvente(float a,float b,float c);

int main() {
	
	float a,b,c;
	//Solicito al usuario los valores
	printf("Introduzca el valor de 'a': ");
    scanf("%f", &a);
	printf("Introduzca el valor de 'b': ");
    scanf("%f", &b);
	printf("Introduzca el valor de 'c': ");
    scanf("%f", &c);
	//Llamo a la función hecha en asm
	formulaResolvente(a,b,c);
	return 0;
}