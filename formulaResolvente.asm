extern printf

section .data

;Variables.
valorMenosB dd 0.0 ;valor de -b
valorNumeradorSuma dd 0.0 ;valor numerador cuando suma
valorNumeradorResta dd 0.0;valor numerador cuando resta
valorRaiz dd 0.0 ;valor parcial de la raiz de la formula
valorB2 dd 0.0 ;valor b elevado a la 2
valor4ac dd 0.0 ;valor 4ac
valor2a dd 0.0 ;valor 2a

;Raices 
raiz1 dq 0.0 ;variable para la raiz1
raiz2 dq 0.0 ;variable para la raiz2

;Valores constantes guardados en variables
const2 dd 2.0
const4 dd 4.0

;El texto a imprimir
mensaje db "Raiz 1 = %.2f y Raiz 2 = %.2f  ", 10,13,0

section .text 

global formulaResolvente
formulaResolvente:
    push ebp ; enter
    mov ebp, esp  ; enter
    
    ;variables ingresadas desde el codigo de C:
    ;a=EBP+8
    ;b=EBP+12
    ;c=EBP+16
    
    ;calculo -b
    FLD dword[EBP+12]
    FCHS ;cambio el signo 
    FSTP dword[valorMenosB] ;guardo en variable y elimino de la fpu  
    
    ;calculo b^2
    FLD dword[EBP+12]
    FLD dword[EBP+12]
    FMUL ;multiplicacion entre ambos valores
    FSTP dword[valorB2] ;guardo en variable y elimino de la fpu 
    
    ;calculo 4ac
    FLD dword[const4] ;cargo la constante 4 en la pila
    FLD dword[EBP+8] ;cargo valor de a
    FMUL ;multiplico
    FLD dword[EBP+16] ;cargo valor de c 
    FMUL ;multiplico
    FSTP dword[valor4ac]  ;guardo en variable y elimino de la fpu  
    
    ;calculo 2a (denominador)
    FLD dword[const2] ;cargo la constante 2 en la pila
    FLD dword[EBP+8];cargo valor de a
    FMUL ;multiplico 
    FSTP dword[valor2a] ;guardo en variable y elimino de la fpu 
    
    ;calculo raiz cuadrada de: b^2 - 4ac
    FLD dword[valorB2]    ;cargo b^2
    FLD dword[valor4ac]    ;cargo 4ac
    FSUB ;realizo la resta
    FSQRT ;al resultado de la resta, le calculo la raiz cuadrada
    FSTP dword[valorRaiz] ;guardo en variable y elimino de la fpu 
    
    ;calculo Numerador Suma 
    FLD dword[valorMenosB] ;cargo -b 
    FLD dword[valorRaiz] ;cargo el resultado de la raiz antes calculado
    FADD  ;hago la suma
    FSTP dword[valorNumeradorSuma] ;guardo en variable y elimino de la fpu
    
    ;calculo Numerador Resta
    FLD dword[valorMenosB] ;cargo -b en la pila
    FLD dword[valorRaiz] ;cargo el resultado de la raiz antes calculado
    FSUB ;hago la resta
    FSTP dword[valorNumeradorResta] ;guardo en variable y elimino de la fpu
    
    ;Realizo el calculo de los numeradores sobre el denominador
    ;luego guardo las raices en su respectiva variable
    
    FLD dword[valorNumeradorSuma]
    FLD dword[valor2a] 
    FDIV ;divido los valores: valorNumeradorSuma/valor2a
    FSTP qword[raiz1] ;guardo en variable y elimino de la fpu
    FLD dword[valorNumeradorResta]
    FLD dword[valor2a] 
    FDIV ;divido los valores:: valorNumeradorResta/valor2a
    FSTP qword[raiz2] ;guardo en variable y elimino de la fpu
    
    ;Imprimo el resultado
    push dword[raiz2+4]
    push dword[raiz2]
    push dword[raiz1+4]
    push dword[raiz1]
    push mensaje
    call printf 
    add esp,20 ;sumo 20 porque cargue en la pila cinco words (4 bytes)
    
    
    mov ebp,esp ;Reset Stack (leave)
    pop ebp ;Restore (leave)
    
    ret
